package com.dgy.util;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcUtils{
    private static final DataSource dataSource;
    static {
        //0 读取配置文件中的参数
        Properties props = new Properties();
        try(InputStream in = JdbcUtils.class.getResourceAsStream("/Jdbc.properties")){
            props.load(in);
            //1 创建DataSource
            try {
                Class.forName(props.getProperty("driver"));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
            String url = props.getProperty("url");
            String username = props.getProperty("uName");
            String password = props.getProperty("pwd");
            dataSource = new DriverManagerDataSource(url, username, password);
        }  catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static JdbcTemplate newJdbcTemplate() {
        //2 创建JdbcTemplate
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    public static DataSource getDataSource(){
        return dataSource;
    }

    //开启事务
    public static Connection startTransaction(){
        Connection conn = null;
        try {
            if(!TransactionSynchronizationManager.isSynchronizationActive()){
                TransactionSynchronizationManager.initSynchronization();
            }
            conn = DataSourceUtils.getConnection(JdbcUtils.getDataSource());
            conn.setAutoCommit(false);// 开启事务
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }

    public static void commit(Connection conn){
        if(conn != null){
            try {
                conn.commit();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }finally{
                clear(conn);
            }
        }
    }

    //回滚事务
    public static void rollback(Connection conn){
        if(conn != null){
            try {
                conn.rollback();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }finally{
                clear(conn);
            }
        }
    }

    //清理功能
    private static void clear(Connection conn){
        TransactionSynchronizationManager.clear();
        TransactionSynchronizationManager.unbindResourceIfPossible(getDataSource());
        DataSourceUtils.releaseConnection(conn, getDataSource());
    }

}
