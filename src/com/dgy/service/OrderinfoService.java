package com.dgy.service;

import com.dgy.entity.Orderinfo;

import java.util.List;

public interface OrderinfoService {
    List<Orderinfo> queryAllOrdeinfos();

    Orderinfo queryOrderinfoById(String oid);

    Boolean addOrderinfo(Orderinfo o);
    Boolean changeOrderinfo(Orderinfo o);
    Boolean removeOrderinfoById(String oid);
}
