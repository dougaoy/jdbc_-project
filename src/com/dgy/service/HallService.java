package com.dgy.service;

import com.dgy.entity.Hall;

import java.util.List;

public interface HallService {
    List<Hall> queryAllHalls();

    Hall queryHallById(Long hid);

    Boolean addHall(Hall h);

    Boolean changeHall(Hall h);

    Boolean removeHallById(Long hid);

}
