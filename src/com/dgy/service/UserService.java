package com.dgy.service;

import com.dgy.entity.User;

import java.util.List;

public interface UserService {
    List<User> queryAllUsres();

    User queryUserById(Long uId);

    Boolean changeUserById(User u);

    Boolean addUser(User u);
    Boolean removeUserById(Long uId);

}
