package com.dgy.service;

import com.dgy.entity.Cinema;

import java.util.List;

public interface CinemaService {
    List<Cinema> queryAllCinemas();

    Cinema queryCinemaById(Long cid);

    Boolean addCinema(Cinema c);
    Boolean changeCinemaById(Cinema c);
    Boolean removeCinemaById(Long cid);


}
