package com.dgy.service;

import com.dgy.entity.Comment;

import java.util.List;

public interface CommentService {
    List<Comment> queryAllComments();

    Comment queryCommentById(Integer cid);

    Boolean addComment(Comment c);

    Boolean changeCommentById(Comment c);

    Boolean removeCommentById(Integer cid);
}
