package com.dgy.service;

import com.dgy.entity.Schedule;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.List;

public interface ScheduleService {
    List<Schedule> queryAllSchedules();

    Schedule queryScheduleById(Long sid);

    Boolean addSchedule(Schedule s);

    Boolean changeSchedule(Schedule s);
    Boolean removeScheduleById(Long sid);


}
