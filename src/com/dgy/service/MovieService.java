package com.dgy.service;

import com.dgy.entity.Movie;
import com.dgy.entity.Schedule;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.List;

public interface MovieService {
    List<Movie> queryAllMovies();

    Movie queryMovieById(Long mid);

    Boolean addMovie(Movie m);
    Boolean changeMovie(Movie m);

    Boolean removeMovieById(Long mid);


}
