package com.dgy.service.impl;

import com.dgy.dao.OrderinfoDao;
import com.dgy.dao.impl.OrderinfoDaoImpl;
import com.dgy.entity.Orderinfo;
import com.dgy.service.OrderinfoService;
import com.dgy.util.JdbcUtils;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class OrderinfoServiceImpl implements OrderinfoService {
  private OrderinfoDao od = new OrderinfoDaoImpl();
    @Override
    public List<Orderinfo> queryAllOrdeinfos() {
        Connection conn = null;
        List<Orderinfo> list = new ArrayList<>();

        try {
            conn = JdbcUtils.startTransaction();
            list =  od.selectAllOrderinfo();
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public Orderinfo queryOrderinfoById(String oid) {
        Connection conn = null;
        Orderinfo orderinfo = null;

        try {
            conn = JdbcUtils.startTransaction();
            orderinfo = od.selectOrderinfoById(oid);
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return orderinfo;
    }

    @Override
    public Boolean addOrderinfo(Orderinfo o) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = od.insertOrderinfo(o);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean changeOrderinfo(Orderinfo o) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = od.updateOrderinfo(o);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean removeOrderinfoById(String oid) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = od.deletOrderinfoById(oid);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }
}
