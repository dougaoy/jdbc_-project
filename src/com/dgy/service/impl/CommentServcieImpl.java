package com.dgy.service.impl;

import com.dgy.dao.CommentDao;
import com.dgy.dao.impl.CommentDaoImpl;
import com.dgy.entity.Comment;
import com.dgy.service.CommentService;
import com.dgy.util.JdbcUtils;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class CommentServcieImpl implements CommentService {
    private CommentDao cd = new CommentDaoImpl();
    @Override
    public List<Comment> queryAllComments() {
        Connection conn = null;
        List<Comment> list = new ArrayList<>();

        try {
            conn = JdbcUtils.startTransaction();
            list =   cd.selectAllComments();
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public Comment queryCommentById(Integer cid) {
        Connection conn = null;
        Comment comment = null;

        try {
            conn = JdbcUtils.startTransaction();
            comment = cd.selectCommentById(cid);
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return comment;
    }

    @Override
    public Boolean addComment(Comment c) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = cd.insertComment(c);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean changeCommentById(Comment c) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = cd.updateCommentById(c);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean removeCommentById(Integer cid) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = cd.deleteCommentById(cid);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }
}
