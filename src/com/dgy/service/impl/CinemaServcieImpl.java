package com.dgy.service.impl;

import com.dgy.dao.CinemaDao;
import com.dgy.dao.impl.CinemaDaoImpl;
import com.dgy.entity.Cinema;
import com.dgy.service.CinemaService;
import com.dgy.util.JdbcUtils;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class CinemaServcieImpl implements CinemaService {
    private CinemaDao cd = new CinemaDaoImpl();
    @Override
    public List<Cinema> queryAllCinemas() {
        Connection conn = null;
        List<Cinema> list = new ArrayList<>();

        try {
            conn = JdbcUtils.startTransaction();
            list = cd.selectAllCinema();
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public Cinema queryCinemaById(Long cid) {
        Connection conn = null;
       Cinema cinema = null;

        try {
            conn = JdbcUtils.startTransaction();
            cinema = cd.selectCinemaById(cid);
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return cinema;
    }

    @Override
    public Boolean addCinema(Cinema c) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = cd.insertCinema(c);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean changeCinemaById(Cinema c) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = cd.updateCinemaById(c);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean removeCinemaById(Long cid) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = cd.deleteCienmaById(cid);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }
}
