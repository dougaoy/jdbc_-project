package com.dgy.service.impl;

import com.dgy.dao.MovieDao;
import com.dgy.dao.impl.MovieDaoImpl;
import com.dgy.entity.Movie;
import com.dgy.service.MovieService;
import com.dgy.util.JdbcUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class MovieServieImpl implements MovieService {
    private MovieDao md = new MovieDaoImpl();
    @Override
    public List<Movie> queryAllMovies() {
        Connection conn = null;
        List<Movie> list = new ArrayList<>();

        try {
            conn = JdbcUtils.startTransaction();
            list = md.selectAllMovie();
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public Movie queryMovieById(Long mid) {
        Connection conn = null;
        Movie movie = null;

        try {
            conn = JdbcUtils.startTransaction();
            movie = md.selectMovieById(mid);
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return movie;
    }

    @Override
    public Boolean addMovie(Movie m) {
        Connection conn = null;
       Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = md.insertMovie(m);
            if(i > 0){
                boo = true;
            }

            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean changeMovie(Movie m) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = md.updateMovieById(m);
            if(i > 0){
                boo = true;
            }

            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean removeMovieById(Long mid) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn = JdbcUtils.startTransaction();
            int i = md.deleteMovieById(mid);
            if(i > 0){
                boo = true;
            }

            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }
}
