package com.dgy.service.impl;

import com.dgy.dao.UserDao;
import com.dgy.dao.impl.UserDaoImpl;
import com.dgy.entity.User;
import com.dgy.service.UserService;
import com.dgy.util.JdbcUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao ud = new UserDaoImpl();

    @Override
    public List<User> queryAllUsres() {
        List<User> list = new ArrayList<>();
        Connection conn = null;
        try {
            conn = JdbcUtils.startTransaction();
            list = ud.selectAllUsers();
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public User queryUserById(Long uId) {
        User user = null;
        Connection conn = null;
        try {
            conn = JdbcUtils.startTransaction();
            user = ud.selectUserById(uId);
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return user;
    }

    @Override
    public Boolean changeUserById(User u) {
        Boolean boo = false;
        Connection conn = null;
        try {
            conn= JdbcUtils.startTransaction();
            if(ud.updateUserById(u)>0) {
                boo =true;
            }else{
                throw new RuntimeException("修改失败");
            }

            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean addUser(User u) {
        Boolean boo = false;
        Connection conn = null;
        try {
            conn= JdbcUtils.startTransaction();
            if(ud.insertUser(u)>0) {
                boo =true;
            }else{
                throw new RuntimeException("添加失败");
            }

            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean removeUserById(Long uId) {
        Boolean boo = false;
        Connection conn = null;
        try {
            conn= JdbcUtils.startTransaction();
            if(ud.deleteUserById(uId)>0) {
                boo =true;
            }else{
                throw new RuntimeException("删除失败");
            }

            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }
}
