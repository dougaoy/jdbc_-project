package com.dgy.service.impl;

import com.dgy.dao.ScheduleDao;
import com.dgy.dao.impl.ScheduleDaoImpl;
import com.dgy.entity.Schedule;
import com.dgy.service.ScheduleService;
import com.dgy.util.JdbcUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class ScheduleServcieImpl implements ScheduleService {
    private ScheduleDao sd = new ScheduleDaoImpl();
    @Override
    public List<Schedule> queryAllSchedules() {
        Connection conn = null;
        List<Schedule> list = new ArrayList<>();

        try {
            conn= JdbcUtils.startTransaction();
            list = sd.selectAllschedles();
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public Schedule queryScheduleById(Long sid) {
        Connection conn = null;
        Schedule schedule = null;

        try {
            conn= JdbcUtils.startTransaction();
            schedule = sd.selectScheduleById(sid);
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return schedule;
    }

    @Override
    public Boolean addSchedule(Schedule s) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn= JdbcUtils.startTransaction();
            int i = sd.insertSchedule(s);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean changeSchedule(Schedule s) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn= JdbcUtils.startTransaction();
            int i = sd.updateSchedule(s);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean removeScheduleById(Long sid) {
        Connection conn = null;
        Boolean boo = false;

        try {
            conn= JdbcUtils.startTransaction();
            int i = sd.deleteScheduleById(sid);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }
}
