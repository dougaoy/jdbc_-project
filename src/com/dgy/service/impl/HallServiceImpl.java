package com.dgy.service.impl;

import com.dgy.dao.HallDao;
import com.dgy.dao.impl.HallDaoImpl;
import com.dgy.entity.Hall;
import com.dgy.service.HallService;
import com.dgy.util.JdbcUtils;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class HallServiceImpl implements HallService {
    private HallDao hd = new HallDaoImpl();

    @Override
    public List<Hall> queryAllHalls() {
        Connection conn = null;
        List<Hall> list = new ArrayList<>();

        try {
            conn = JdbcUtils.startTransaction();
            list = hd.selectAllHalls();
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return list;
    }

    @Override
    public Hall queryHallById(Long hid) {
        Connection conn = null;
        Hall hall = null;
        try {
            conn = JdbcUtils.startTransaction();
            hall = hd.selectHallById(hid);
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return hall;
    }

    @Override
    public Boolean addHall(Hall h) {
        Connection conn = null;
        Boolean boo = null;
        try {
            conn = JdbcUtils.startTransaction();
            int i = hd.insertHall(h);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean changeHall(Hall h) {
        Connection conn = null;
        Boolean boo = null;
        try {
            conn = JdbcUtils.startTransaction();
            int i = hd.updateHallById(h);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;
    }

    @Override
    public Boolean removeHallById(Long hid) {
        Connection conn = null;
        Boolean boo = null;
        try {
            conn = JdbcUtils.startTransaction();
            int i = hd.deleteHallById(hid);
            if(i>0){
                boo = true;
            }
            JdbcUtils.commit(conn);
        } catch (Exception e) {
            JdbcUtils.rollback(conn);
            throw new RuntimeException(e);
        }
        return boo;

    }
}
