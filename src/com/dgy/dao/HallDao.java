package com.dgy.dao;

import com.dgy.entity.Hall;

import java.util.List;

public interface HallDao  {

    /**
     * 查询所有Hall
     * @return 包含了所有hall对象的集合
     */
    List<Hall> selectAllHalls();

    /**
     * 根据hall_id查询hall对象
     * @param hId 要查询的hall对象的id
     * @return 查找到的hall对象
     */
    Hall selectHallById(Long hId);

    /**
     * 向hall表中添加数据
     * @param h 包含了要添加的hall对象数据的对象
     * @return 受影响的行数
     */
    int insertHall(Hall h);

    /**
     * 根据Hall_id 删除Hall对象
     * @param hId 要删除的对象的hall_id
     * @return 受影响的行数
     */
    int deleteHallById(Long hId);

    /**
     * 修改hall对象的数据
     * @param h 包含了要修改的hall对象的数据的对象
     * @return 受影响的行数
     */
    int updateHallById(Hall h);
}
