package com.dgy.dao;

import com.dgy.entity.Orderinfo;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.List;

public interface OrderinfoDao {

    /**
     * 查询所有orderinfo
     * @return 包含了所有orderinfo的集合
     */
    List<Orderinfo> selectAllOrderinfo();

    /**
     * 根据oid查询orderinfo对象
     * @param oid 要查询的orderinfo对象的id
     * @return 找到的orderinfo对象
     */
    Orderinfo selectOrderinfoById(String oid);

    /**
     * 向orderinfo表中添加数据
     * @param o 包含了要添加的orderinfo的数据的对象
     * @return 受影响的行数
     */
    int insertOrderinfo(Orderinfo o);

    /**
     * 修改orderinfo表的数据
     * @param o 包含了要修改的数据的orderinfo的对象
     * @return 受影响的行数
     */
    int updateOrderinfo(Orderinfo o);

    /**
     * 根据oid删除orderinfo对象
     * @param oid 要删除的对象的id
     * @return 受影响的行数
     */
    int deletOrderinfoById(String oid);
}
