package com.dgy.dao;

import com.dgy.entity.Schedule;

import java.util.List;

public interface ScheduleDao  {
    /**
     *查询所有shchedules对象
     * @return 包含了所有schedules对象的集合
     */
    List<Schedule> selectAllschedles();

    /**
     * 根据id查询schedule对象
     * @param sId 要查询的对象的sid
     * @return 找到的schedule对象
     */
    Schedule selectScheduleById(Long sId);

    /**
     * 向schedule表中添加数据
     * @param s 包含了要添加的schedule对象数据的对象
     * @return  受影响的行数
     */
    int insertSchedule(Schedule s);

    /**
     * 更新schedule表中数据
     * @param s 包含了要更新的内容的对象
     * @return  受影响的行数
     */
    int updateSchedule(Schedule s);

    /**
     * 根据id删除schedule
     * @param sid 要删除的schedule对象的id
     * @return 受影响的行数
     */
    int deleteScheduleById(Long sid);



}
