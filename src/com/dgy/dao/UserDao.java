package com.dgy.dao;

import com.dgy.entity.User;

import java.util.List;

public interface UserDao {
    /**
     * 查询所有User对象
     * @return 包含了所有user对象的集合
     *
     */
    List<User> selectAllUsers();

    /**
     * 根据use_id查询user对象
     * @param uId 要查询的user对象的id
     * @return 找到的user对象
     */
    User selectUserById(Long uId);

    /**
     * 添加User对象
     * @param u 包含了要添加的User对象的对象
     * @return 受影响的行数
     */
    int insertUser(User u);

    /**
     * 根据userId删除 user对象
     * @param uId 要删除的user对象的id
     * @return 受影响的行数
     */
    int deleteUserById(Long uId);

    /**
     * 根据userId修改user对象
     * @param u 包含了要修改的对象数据的对象
     * @return 受影响的行数
     */
    int updateUserById(User u);
}
