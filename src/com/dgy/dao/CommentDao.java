package com.dgy.dao;

import com.dgy.entity.Comment;
import javafx.collections.ObservableIntegerArray;

import java.util.List;

public interface CommentDao {

    /**
     * 查询所有Comment对象
     * @return 包含了所有comment对象的集合
     */
    List<Comment> selectAllComments();

    /**
     * 根据cid查找Comment对象
     * @param cid 要查询的comment对象的comment_id
     * @return  找到的comment对象
     *
     */
    Comment selectCommentById(Integer cid);

    /**
     * 向comment表中添加数据
     * @param c 包含了要添加的comment对象的数据的对象
     * @return 受影响的行数
     */
    int insertComment(Comment c);

    /**
     * 根据id删除Comment对象
     * @param id 要删除的comment对象的id
     * @return 收影响的行数
     */
    int deleteCommentById(Integer id);

    /**
     * 修改comment对象
     * @param c 包含了要修改的comment对象的数据的对象
     * @return 受影响的行数
     */
    int updateCommentById(Comment c);

}
