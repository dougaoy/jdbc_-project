package com.dgy.dao.impl;

import com.dgy.dao.MovieDao;
import com.dgy.entity.Movie;
import com.dgy.util.JdbcUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class MovieDaoImpl implements MovieDao {
    private JdbcTemplate jdbcTemplate = JdbcUtils.newJdbcTemplate();
    @Override
    public List<Movie> selectAllMovie() {
        String sql = " select * from movie";
        return jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(Movie.class));
    }

    @Override
    public Movie selectMovieById(Long mid) {
        String sql = " select * from movie where  movie_id = ?";
        List<Movie> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Movie.class), mid);
        return list.isEmpty()?null:list.get(0);
    }

    @Override
    public int insertMovie(Movie m) {
        String sql = "insert into movie values(null,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql,m.getMovieCnName(),m.getMovieFgName(),m.getMovieActor(),m.getMovieDirector(),m.getMovieDetail(),m.getMovieDuration(),m.getMovieType(),m.getMovieScore(),m.getMovieBoxOffice(),m.getMovieCommentCount(),m.getMovieReleaseDate(),m.getMoviePicture(),m.getMovieCountry(),m.getMovieState());
    }

    @Override
    public int updateMovieById(Movie m) {
        String sql ="update movie set movie_cn_name = ?,movie_fg_name = ?,movie_actor = ?,movie_director = ?,movie_detail = ?,movie_duration = ?,movie_type = ?,movie_score = ?,movie_box_office = ?,movie_comment_count = ?,movie_release_date = ?,movie_picture = ?,movie_country = ?,movie_state = ? where movie_id = ?";
        return jdbcTemplate.update(sql,m.getMovieCnName(),m.getMovieFgName(),m.getMovieActor(),m.getMovieDirector(),m.getMovieDetail(),m.getMovieDuration(),m.getMovieType(),m.getMovieScore(),m.getMovieBoxOffice(),m.getMovieCommentCount(),m.getMovieReleaseDate(),m.getMoviePicture(),m.getMovieCountry(),m.getMovieState(),m.getMovieId());
    }

    @Override
    public int deleteMovieById(Long mid) {
        String sql = "delete from movie where movie_id = ?";
        return jdbcTemplate.update(sql,mid);
    }
}
