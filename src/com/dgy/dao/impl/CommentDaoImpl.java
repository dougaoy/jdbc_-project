package com.dgy.dao.impl;

import com.dgy.dao.CommentDao;
import com.dgy.entity.Comment;
import com.dgy.util.JdbcUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class CommentDaoImpl implements CommentDao {
    private JdbcTemplate jdbcTemplate = JdbcUtils.newJdbcTemplate();

    @Override
    public List<Comment> selectAllComments() {
        String sql ="select * from comment";
        return jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(Comment.class));
    }

    @Override
    public Comment selectCommentById(Integer cid) {
        String sql = "select * from comment where comment_id = ?";
        List<Comment> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Comment.class), cid);
        return list.isEmpty()?null:list.get(0);
    }

    @Override
    public int insertComment(Comment c) {
        String sql = "insert into comment values(null,?,?,?,?,?)";
        return jdbcTemplate.update(sql,c.getUserId(),c.getCommentContent(),c.getMovieId(),c.getCommentTime(),c.getCommentScore());
    }

    @Override
    public int deleteCommentById(Integer id) {
        String sql = "delete from comment where comment_id = ?" ;
        return jdbcTemplate.update(sql,id);
    }

    @Override
    public int updateCommentById(Comment c) {
        String sql = "update comment set user_id = ?,comment_content = ?,movie_id = ?,comment_time = ?,comment_score = ? where comment_id = ?";
        return jdbcTemplate.update(sql,c.getUserId(),c.getCommentContent(),c.getMovieId(),c.getCommentTime(),c.getCommentScore(),c.getCommentId());
    }
}
