package com.dgy.dao.impl;

import com.dgy.dao.OrderinfoDao;
import com.dgy.entity.Orderinfo;
import com.dgy.util.JdbcUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class OrderinfoDaoImpl implements OrderinfoDao {
    private JdbcTemplate jdbcTemplate = JdbcUtils.newJdbcTemplate();
    @Override
    public List<Orderinfo> selectAllOrderinfo() {
        String sql = "select * from orderinfo";
        return jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(Orderinfo.class));
    }

    @Override
    public Orderinfo selectOrderinfoById(String oid) {
        String sql = "select * from orderinfo where order_id = ? ";
        List<Orderinfo> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Orderinfo.class), oid);
        return list.isEmpty()?null:list.get(0);
    }

    @Override
    public int insertOrderinfo(Orderinfo o) {
        String sql = "insert into orderinfo values(?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql,o.getOrderId(),o.getUserId(),o.getScheduleId(),o.getOrderPosition(),o.getOrderState(),o.getOrderPrice(),o.getOrderTime());
    }

    @Override
    public int updateOrderinfo(Orderinfo o) {
        String sql = "update orderinfo set user_id = ?,schedule_id = ?, order_position = ?,order_state = ?,order_price = ?,order_time = ? where order_id = ?";

        return jdbcTemplate.update(sql,o.getUserId(),o.getScheduleId(),o.getOrderPosition(),o.getOrderState(),o.getOrderPrice(),o.getOrderTime(),o.getOrderId());
    }

    @Override
    public int deletOrderinfoById(String oid) {
        String sql = "delete from orderinfo where order_id = ?";
        return jdbcTemplate.update(sql,oid);
    }
}
