package com.dgy.dao.impl;

import com.dgy.dao.HallDao;
import com.dgy.entity.Hall;
import com.dgy.util.JdbcUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class HallDaoImpl implements HallDao {
private JdbcTemplate jdbcTemplate = JdbcUtils.newJdbcTemplate();
    @Override
    public List<Hall> selectAllHalls() {
        String sql = "select * from hall";
        return jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(Hall.class));
    }

    @Override
    public Hall selectHallById(Long hId) {
        String sql = "select * from hall where hall_id = ?";
        List<Hall> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Hall.class), hId);
        return list.isEmpty()?null:list.get(0);
    }

    @Override
    public int insertHall(Hall h) {
        String sql = "insert into hall values(null,?,?,?)";
        return jdbcTemplate.update(sql,h.getHallName(),h.getHallCapacity(),h.getCinemaId());
    }

    @Override
    public int deleteHallById(Long hId) {
        String sql = "delete from hall where hall_id = ?";
        return jdbcTemplate.update(sql,hId);
    }

    @Override
    public int updateHallById(Hall h) {
        String sql = "update hall set hall_name = ?,hall_capacity = ?,cinema_id = ? where hall_id = ?";
        return jdbcTemplate.update(sql,h.getHallName(),h.getHallCapacity(),h.getCinemaId(),h.getHallId());
    }
}
