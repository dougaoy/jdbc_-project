package com.dgy.dao.impl;

import com.dgy.dao.CinemaDao;
import com.dgy.entity.Cinema;
import com.dgy.util.JdbcUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class CinemaDaoImpl implements CinemaDao {
    private JdbcTemplate jdbcTemplate = JdbcUtils.newJdbcTemplate();
    @Override
    public List<Cinema> selectAllCinema() {
        String sql ="select * from cinema";
        return jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(Cinema.class));
    }

    @Override
    public Cinema selectCinemaById(Long cId) {
        String sql = "select * from cinema where cinema_id = ?";
        List<Cinema> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Cinema.class), cId);
        return list.isEmpty()?null:list.get(0);
    }

    @Override
    public int insertCinema(Cinema c) {
        String  sql = "insert into cinema values(null,?,?)";
        return jdbcTemplate.update(sql,c.getCinemaName(),c.getCinemaAddress());
    }

    @Override
    public int deleteCienmaById(Long cId) {
        String sql = "delete from cinema where cinema_id = ?";
        return jdbcTemplate.update(sql,cId);
    }

    @Override
    public int updateCinemaById(Cinema c) {
        String sql = "update cinema set cinema_name = ? ,cinema_address = ? where cinema_id = ?";
        return jdbcTemplate.update(sql,c.getCinemaName(),c.getCinemaAddress(),c.getCinemaId());
    }
}
