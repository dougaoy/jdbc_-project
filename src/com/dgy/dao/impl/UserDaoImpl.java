package com.dgy.dao.impl;

import com.dgy.dao.UserDao;
import com.dgy.entity.User;
import com.dgy.util.JdbcUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class UserDaoImpl implements UserDao {
    private JdbcTemplate jdbcTemplate = JdbcUtils.newJdbcTemplate();
    @Override
    public List<User> selectAllUsers() {
        String sql = "select * from user ";
        return jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(User.class));
    }

    @Override
    public User selectUserById(Long uId) {
        String sql = "select * from user where user_id = ?";
        List<User> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(User.class), uId);
        return list.isEmpty()?null:list.get(0);
    }

    @Override
    public int insertUser(User u) {
        String sql ="insert into user values(null,?,?,?,?,?)";
        return jdbcTemplate.update(sql,u.getUserName(),u.getUserPwd(),u.getUserEmail(),u.getUserRole(),u.getUserHeadImg());
    }

    @Override
    public int deleteUserById(Long uId) {
        String sql = "delete from user where user_id = ?";
        return jdbcTemplate.update(sql,uId);
    }

    @Override
    public int updateUserById(User u) {
        String sql = "update user set user_name = ?, user_pwd = ?,user_email = ?,user_role = ?,user_head_img = ? where user_id = ?";
        return jdbcTemplate.update(sql,u.getUserName(),u.getUserPwd(),u.getUserEmail(),u.getUserRole(),u.getUserHeadImg(),u.getUserId());
    }
}
