package com.dgy.dao.impl;

import com.dgy.dao.ScheduleDao;
import com.dgy.entity.Schedule;
import com.dgy.util.JdbcUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class ScheduleDaoImpl implements ScheduleDao {
    private JdbcTemplate jdbcTemplate = JdbcUtils.newJdbcTemplate();
    @Override
    public List<Schedule> selectAllschedles() {
        String sql = "select * from schedule ";

        return jdbcTemplate.query(sql,new BeanPropertyRowMapper<>(Schedule.class));
    }

    @Override
    public Schedule selectScheduleById(Long sId) {
        String sql = "select * from schedule  where schedule_id = ?";
        List<Schedule> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Schedule.class), sId);
        return list.isEmpty()?null:list.get(0);
    }

    @Override
    public int insertSchedule(Schedule s) {
        String sql = "insert into schedule values(null,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql,s.getHallId(),s.getMovieId(),s.getScheduleStartTime(),s.getSchedulePrice(),s.getScheduleRemain(),s.getScheduleState());
    }

    @Override
    public int updateSchedule(Schedule s) {
        String sql = "update schedule set hall_id = ?,movie_id = ?,schedule_start_time = ?,schedule_price = ?,schedule_remain = ?,schedule_state = ? where schedule_id = ?";
        return jdbcTemplate.update(sql,s.getHallId(),s.getMovieId(),s.getScheduleStartTime(),s.getSchedulePrice(),s.getScheduleRemain(),s.getScheduleState(),s.getScheduleId());

    }

    @Override
    public int deleteScheduleById(Long sid) {
        String sql = "delete from schedule where schedule_id = ?";
        return jdbcTemplate.update(sql,sid);
    }
}
