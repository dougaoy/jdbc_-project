package com.dgy.dao;

import com.dgy.entity.Movie;

import java.util.List;

public interface MovieDao {

    /**
     * 查询所有movie
     * @return 查找到的movie的结果集
     */
    List<Movie> selectAllMovie();

    /**
     * 根据movie_id查询movie对象
     * @param mid 要查询的movie对象的id
     * @return 找到的movie对象
     */
    Movie selectMovieById(Long mid);

    /**
     * 向movie表中添加数据
     * @param m 包含了要添加的数据的对象
     * @return 受影响的行数
     */
    int insertMovie(Movie m);

    /**
     * 修改movie对象的数据
     * @param m 包含了要修改的数据的对象
     * @return 受影响的行数
     */
    int updateMovieById(Movie m);

    /**
     * 根据movie_id删除movie对象
     * @param mid 要删除的movie对象的id
     * @return 受影响的行数
     */
    int deleteMovieById(Long mid);
}
