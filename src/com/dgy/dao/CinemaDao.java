package com.dgy.dao;

import com.dgy.entity.Cinema;

import java.util.List;

public interface CinemaDao {
    /**
     * 查询所有cinema
     * @return 包含了所有cinema对象的集合
     */
    List<Cinema> selectAllCinema();

    /**
     * 根据Cinema_id查询cinema对象
     * @param cId 要查询的对象的cinema_ida
     * @return 找到的cienma的对象
     */
    Cinema selectCinemaById(Long cId);

    /**
     * 向cinema中添加对象
     * @param c 包含了要添加的cienma对象的cinema对象
     * @return 受影响的行数
     */
    int insertCinema(Cinema c);

    /**
     * 根据cienma_id删除cinema对象
     * @param cId 要删除的cienma对象的id
     * @return 受影响的行数
     */
    int deleteCienmaById(Long cId);

    /**
     * 根据cinema_Id修改cienma对象
     * @param c 包含了要修改得到数据的cinema对象
     * @return 受影响的行数
     */
    int updateCinemaById(Cinema c);
}
