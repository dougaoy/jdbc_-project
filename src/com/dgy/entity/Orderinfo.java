package com.dgy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Orderinfo implements Serializable {
    private String orderId;
    private Long userId;
    private Long scheduleId;
    private String orderPosition;
    private Integer orderState;
    private Integer orderPrice;
    private Date orderTime;
}
