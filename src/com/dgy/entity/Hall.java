package com.dgy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hall implements Serializable {
    private Long hallId;
    private String hallName;
    private Integer hallCapacity;
    private Long cinemaId;
}
