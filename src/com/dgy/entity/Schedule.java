package com.dgy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Schedule implements Serializable {
    private Long scheduleId;
    private Long hallId;
    private Long movieId;
    private String scheduleStartTime;
    private Integer schedulePrice;
    private Integer scheduleRemain;
    private Integer scheduleState;
}
