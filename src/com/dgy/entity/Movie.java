package com.dgy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Movie implements Serializable {
    private Long movieId;
    private String movieCnName;
    private String movieFgName;
    private String movieActor;
    private String movieDirector;
    private String movieDetail;
    private String movieDuration;
    private String movieType;
    private Float movieScore;
    private Float movieBoxOffice;
    private Long movieCommentCount;
    private Date movieReleaseDate;
    private String moviePicture;
    private String movieCountry;
    private Integer movieState;

}
