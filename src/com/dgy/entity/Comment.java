package com.dgy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment implements Serializable {
    private Integer commentId;
    private Integer userId;
    private String commentContent;
    private Integer movieId;
    private Date commentTime;
    private Integer commentScore;
}
