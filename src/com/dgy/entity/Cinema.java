package com.dgy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cinema implements Serializable {
    private Long cinemaId;
    private String cinemaName;
    private String cinemaAddress;

}
