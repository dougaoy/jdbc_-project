package com.dgy.test;

import com.dgy.entity.Cinema;
import com.dgy.service.CinemaService;
import com.dgy.service.impl.CinemaServcieImpl;
import org.junit.Test;

public class CinemaServiceTest {
    private CinemaService cs = new CinemaServcieImpl();
    @Test
    public void queryAllCinemasTest(){
        cs.queryAllCinemas().forEach(System.err::println);
    }

    @Test
    public void queryCinemaTest(){
        System.out.println(cs.queryCinemaById(2l));

    }

    @Test
    public void addCinemaTest(){
        if (cs.addCinema(new Cinema(null,"天空影院","冯祥街早晨阳光城"))) {
            System.out.println("添加成功");
        }else{
            System.out.println("添加失败");
        }
    }
    @Test
    public void changeCinemaTest(){
        if (cs.changeCinemaById(new Cinema(8l,"天空影院","冯祥街早晨阳光城"))) {
            System.out.println("修改成功");
        }else{
            System.out.println("修改失败");
        }
    }

    @Test
    public void removeCinemaByIdTest(){
        if (cs.removeCinemaById(8l)) {
            System.out.println("删除成功");
        }else {
            System.out.println("删除失败");
        }
    }
}
