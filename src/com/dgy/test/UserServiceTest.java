package com.dgy.test;

import com.dgy.entity.User;
import com.dgy.service.UserService;
import com.dgy.service.impl.UserServiceImpl;
import org.junit.Test;

import java.util.List;

public class UserServiceTest {
    private UserService us = new UserServiceImpl();
    @Test
    public void queryAllUserTest(){
        List<User> users = us.queryAllUsres();
        users.forEach(System.err::println);
    }
    @Test
    public void queryUserByIdTest(){
        System.out.println(us.queryUserById(2l));
    }

    @Test
    public void removeUserByIdTest(){
        if (us.removeUserById(2l)) {
            System.out.println("删除成功");
        }
    }
    @Test
    public void addUserTest(){
        if (us.addUser(new User(null,"jack","123456","2732192697@qq.com",1,null))) {
            System.out.println("添加成功");
        }
    }
    @Test
    public void changeUserTest(){
        if (us.changeUserById(new User(3l,"jack","123456","2732192697@qq.com",0,null))) {
            System.out.println("修改成功");
        }
    }

}
