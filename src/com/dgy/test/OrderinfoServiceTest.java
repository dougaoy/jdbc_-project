package com.dgy.test;

import com.dgy.dao.OrderinfoDao;
import com.dgy.entity.Orderinfo;
import com.dgy.service.OrderinfoService;
import com.dgy.service.impl.OrderinfoServiceImpl;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderinfoServiceTest {

    private OrderinfoService os = new OrderinfoServiceImpl();

    @Test
    public void queryAllOrderinfoTest(){
        os.queryAllOrdeinfos().forEach(System.err::println);
    }

    @Test
    public void queryOrderinfoByIdTest(){
        System.out.println(os.queryOrderinfoById("2020010100010607"));
    }

    @Test
    public void addOrderinfoTest(){
        String date1 = "2001-10-27 12:23:98";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(date1);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        if (os.addOrderinfo(new Orderinfo("1",1l,2l,"3排4座",2,90,date))) {
            System.out.println("添加成功");
        }else{
            System.out.println("添加失败");
        }
    }
    @Test
    public void changeOrderinfoTest(){
        String date1 = "2001-10-27 12:23:98";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(date1);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        if (os.changeOrderinfo(new Orderinfo("1",1l,2l,"3排4座",2,120,date))) {
            System.out.println("修改成功");
        }else{
            System.out.println("修改失败");
        }
    }

    @Test
    public void removeOrderinfoTest(){
        if (os.removeOrderinfoById("1")) {
            System.out.println("删除成功");
        }else{
            System.out.println("删除失败");
        }
    }
}
