package com.dgy.test;

import com.dgy.entity.Movie;
import com.dgy.service.MovieService;
import com.dgy.service.impl.MovieServieImpl;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MovieServiceTest {
    private MovieService ms = new MovieServieImpl();

    @Test
    public void queryAllMoviesTest(){
        ms.queryAllMovies().forEach(System.err::println);
    }

    @Test
    public void queryMovieByIdTest(){
        System.out.println(ms.queryMovieById(6l));
    }

    @Test
    public void addMovieTest(){
        String date1 = "2001-10-27 00:00:00";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(date1);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        if (ms.addMovie(new Movie(null,"国产凌凌漆"," 007 made in China","周星驰","周星驰","一位名不见经传的特工零零七为找出偷取恐龙骨架的坏人的故事","120分钟","喜剧片",8.9f,null,21l,date,"/file/upload/head/4785910d-b988-41b1-8168-ac18294021eb.jpg","中国",1))) {
            System.out.println("添加成功");
        }else{
            System.out.println("添加失败");
        }
    }

    @Test
    public void changeMovieTest(){
        String date1 = "2001-10-27 00:00:00";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(date1);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        if (ms.changeMovie((new Movie(24l,"国产凌凌漆"," 007 made in China","周星驰","周星驰","一位名不见经传的特工零零七为找出偷取恐龙骨架的坏人的故事","120分钟","喜剧片",8.9f,null,21l,date,"/file/upload/head/4785910d-b988-41b1-8168-ac18294021eb.jpg","中国香港",1)))) {
            System.out.println("修改成功");
        }else{
            System.out.println("修改失败");
        }
    }

}
