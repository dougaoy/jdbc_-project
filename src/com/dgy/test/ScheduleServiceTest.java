package com.dgy.test;

import com.dgy.entity.Schedule;
import com.dgy.service.ScheduleService;
import com.dgy.service.impl.ScheduleServcieImpl;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScheduleServiceTest {
    private ScheduleService ss = new ScheduleServcieImpl();
    @Test
    public void queryAllSchedulesTest(){
        ss.queryAllSchedules().forEach(System.err::println);
    }
    @Test
    public void queryScheduleTest(){
        System.out.println(ss.queryScheduleById(1l));
    }

    @Test
    public void addScheduleTest(){
        String date1 = "2001-10-27 05:00";
        if (ss.addSchedule(new Schedule(null,3l,2l,date1,100,2,1))) {
            System.out.println("添加成功");
        }else{
            System.out.println("添加失败");
        }

    }
    @Test
    public void changeScheduleTest(){
        String date1 = "2001-10-27 05:10";
        if (ss.changeSchedule(new Schedule(44l,3l,2l,date1,100,2,1))) {
            System.out.println("修改成功");
        }else{
            System.out.println("修改失败");
        }
    }
    @Test
    public void removeScheduleTest(){
        if (ss.removeScheduleById(45l)) {
            System.out.println("删除成功");
        }else{
            System.out.println("删除失败");
        }
    }

}
