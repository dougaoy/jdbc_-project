package com.dgy.test;

import com.dgy.entity.Comment;
import com.dgy.service.CommentService;
import com.dgy.service.impl.CommentServcieImpl;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommentServiceTest {
    private CommentService cs = new CommentServcieImpl();

    @Test
    public void queryAllCommentsTest(){
        cs.queryAllComments().forEach(System.err::println);
    }

    @Test
    public void queryCommentByIdTest(){
        System.out.println(cs.queryCommentById(1));
    }

    @Test
    public void addCommentTest(){
        String date1 = "2001-10-27 10:10:10";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
          date = simpleDateFormat.parse(date1);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        Boolean boo = cs.addComment(new Comment(null, 3, "123231221", 2, date, 3));
        if(boo){
            System.out.println("添加成功");
        }else{
            System.out.println("添加失败");
        }


    }
    @Test
    public void changeCommentTest(){
        String date1 = "2001-10-27 10:10:10";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(date1);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        if (cs.changeCommentById(new Comment(9, 3, "123231221", 2, date, 3))) {
            System.out.println("修改成功");
        }else{
            System.out.println("修改失败");
        }

    }

    @Test
    public void removeCommentByIdTest(){
        if (cs.removeCommentById(9)) {
            System.out.println("删除成功");
        }else{
            System.out.println("删除失败");
        }
    }
}
