package com.dgy.test;

import com.dgy.entity.Hall;
import com.dgy.service.HallService;
import com.dgy.service.impl.HallServiceImpl;
import org.junit.Test;

public class HallServiceTest {
    private HallService hs = new HallServiceImpl();

    @Test
    public void queryAllHallTest(){
        hs.queryAllHalls().forEach(System.err::println);
    }

    @Test
    public void queryHallByIdTest(){
        System.out.println(hs.queryHallById(21l));

    }

    @Test
    public void addHallTest(){
        if (hs.addHall(new Hall(null,"二号厅",122,1l))) {
            System.out.println("添加成功");
        }else{
            System.out.println("添加失败");
        }
    }
    @Test
    public void changeHallTest(){
        if (hs.changeHall(new Hall(2l,"三号厅",100,1l))) {
            System.out.println("修改成功");
        }else{
            System.out.println("修改失败");
        }
    }

    @Test
    public void removeHallTest(){
        if (hs.removeHallById(2l)) {
            System.out.println("删除成功");
        }else{
            System.out.println("删除失败");
        }
    }
}
